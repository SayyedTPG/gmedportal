package com.rt.test;

import static com.rt.helper.DriverFactory.driver;
import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;
import com.rt.pages.authenticated.MainPage;
import com.rt.pages.unauthenticated.LoginPage;
import com.rt.test.base.AuthBaseTestClass;
import com.rt.test.base.BaseTestClass;
import com.rt.testConfig.TestProperty;
import com.rt.testConfig.TestProperty;
import com.rt.utils.ExcelReaderUtilityForDataProvider;
import com.rt.utils.SeleniumUtil;
import static com.rt.helper.DriverFactory.driver;

/**
 * This class is the test class of PortalLoginPage class
 * @author sayyed.akram
 *
 */

public class LoginPageTest extends AuthBaseTestClass {

	
	private static final String VALID_USERID 				= "validUserName";
	private static final String VALID_PASSWORD 				= "validPassword";
	private static final String INVALID_USERID				= "invalidUserName";
	private static final String INVALID_PASSWORD			= "invalidPassword";
	private static final String USER_NAME_BLANK				= "userNameBlankErrorMessage";
	private static final String PASSWORD_BLANK				= "passwordBlankErrorMessage";
	public static Logger logger = LogManager.getLogger(LoginPageTest.class); 
	private Map<String, String> portalLoginPageData;
	private LoginPage loginPageObj ;
	private MainPage mainPageObj;
	private Assertion loginPageAssert = new Assertion();
	
	@BeforeClass(alwaysRun = true)
	public void initClass() throws InterruptedException{
		
		portalLoginPageData = readexcelsheet("login");
		loginPageObj = new LoginPage();
		mainPageObj = new MainPage();
	} 

	@Test(dataProvider = "LoginDataProvider")
	public void multipleValidLogin(String userName, String password) throws InterruptedException  {	
		
		doLogin(userName,password);
		Thread.sleep(5000);
		mainPageObj.clickLogOffLink();
	}

   @AfterClass(alwaysRun = true)
	public void TearDown(){
    	loginPageObj = null;
    	
    }
   
	@DataProvider(name = "LoginDataProvider")
	public Object [][] getLoginData() throws IOException {

		ExcelReaderUtilityForDataProvider dataProviderUtility = new ExcelReaderUtilityForDataProvider();
		Object data [][] = dataProviderUtility.getTestData("DataSheet.xlsx", "login");
		return data;
		}
	
}
