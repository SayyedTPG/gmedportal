package com.rt.test.base;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import com.rt.testConfig.TestProperty;
import com.rt.utils.ExcelFileUtility;
import static com.rt.helper.DriverFactory.*;

public class BaseTestClass {
	
	private static Logger logger  = LogManager.getLogger(com.rt.test.base.BaseTestClass.class);
	
	@BeforeClass()
	public void initTest(){
		
		getDriverInstance();
		openURL(TestProperty.TESTURL);
	}
	
	protected Map<String, String> readexcelsheet(String sheetName){
		
		ExcelFileUtility testDataObj = new ExcelFileUtility();
		return testDataObj.readExcelFile(TestProperty.WORKBOOKNAME, sheetName);		
	}

	@AfterClass()
	public void cleanupclass(){
		
		//driver.quit();
	}
}
