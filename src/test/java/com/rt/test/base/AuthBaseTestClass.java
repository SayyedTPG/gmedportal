package com.rt.test.base;


import com.rt.pages.unauthenticated.LoginPage;

public class AuthBaseTestClass extends BaseTestClass{

	
	public void doLogin(String... substitutionValue){
		String userName 	= substitutionValue[0];
		String password 	= substitutionValue[1];
		
	LoginPage loginPageObj = new LoginPage();
	loginPageObj.loginAs(userName,password);
	
	}
}
