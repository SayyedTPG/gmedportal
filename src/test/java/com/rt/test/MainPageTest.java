package com.rt.test;

import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;

import com.rt.pages.authenticated.MainPage;
import com.rt.test.base.AuthBaseTestClass;
import com.rt.utils.SeleniumUtil;

public class MainPageTest extends AuthBaseTestClass {

	public static final String VALID_USERID 				= "validUserName";
	public static final String VALID_PASSWORD 				= "validPassword";
	public static final String HOME 						= "homepagetitle";
	public static final String MESSAGES 					= "messagepagetitle";
	public static final String MY_HEALTH_SUMMARY 			= "healthsummarypagetitle";
	public static final String MY_DOCUMENTS 				= "mydocumentspagetitle";
	public static final String ONLINE_BILL_PAY 				= "onlinebillpaytitle";
	public static final String CHANGE_USERNAME 				= "changeusernamepagetitle";
	public static final String CHANGE_PASSWORD 				= "changepasswordpagetitle";
	public static final String GRANT_ACCESS 				= "grantaccesspagetitle";
	public static final String HISTORY_LOG 					= "historylogpagetitle";
	public static final String LOG_OFF 						= "loggoff";

	private MainPage mainPage;
	private Assertion mainPageAssert = new Assertion();
	private Map<String, String> MainPageData;

	@BeforeClass
	public void initClass() throws InterruptedException {
		MainPageData = readexcelsheet("mainpage");
		mainPage = new MainPage();
		doLogin(MainPageData.get(VALID_USERID), MainPageData.get(VALID_PASSWORD));
	}

	@Test(description = "This Test Case is to Check the Title of Home Page", priority=1,groups = { "sprint2" })
	public void ToVerifykHomePageTitle() throws InterruptedException {

		mainPage.clickHomePageLink();
		mainPageAssert.assertEquals(SeleniumUtil.getPagetitle(),MainPageData.get(HOME) );
				
	}

	@Test(description = "This Test Case is to Check the Title of Messages Page", priority=2,groups = { "sprint2" })
	public void ToVerifySentMessagesPageTitle() {

		mainPage.clickOnSentMessagesLink();
		mainPageAssert.assertEquals(SeleniumUtil.getPagetitle(),MainPageData.get(MESSAGES) );
	}

	@Test(description = "This Test Case is to Check the Title of HealthSummary Page", priority=3,groups = { "sprint2" })
	public void ToVerifyHealthSummaryPageTitle() {

		mainPage.clickOnHealthSummaryLink();
		mainPageAssert.assertEquals(SeleniumUtil.getPagetitle(),MainPageData.get(MY_HEALTH_SUMMARY) );
	}

	@Test(description = "This Test Case is to Check the Title of Mydocument Page", priority=4,groups = { "sprint2" })
	public void ToVerifyMydocumentPageTitle() {

		mainPage.clickOnMydocumentLink();
		mainPageAssert.assertEquals(SeleniumUtil.getPagetitle(),MainPageData.get(MY_DOCUMENTS) );
	}

	@Test(description = "This Test Case is to Check the Title of BillPay Page", priority=5,groups = { "sprint2" })
	public void ToVerifBillPayPageTitle() {

		mainPage.clickOnBillPayLink();
		mainPageAssert.assertEquals(SeleniumUtil.getPagetitle(),MainPageData.get(ONLINE_BILL_PAY) );
	}

	@Test(description = "This Test Case is to Check the Title of ChangeUserName Page", priority=6,groups = { "sprint2" })
	public void ToVerifyChangeUserNamePageTitle() {

		mainPage.clickOnChangeUserNameLink();
		mainPageAssert.assertEquals(SeleniumUtil.getPagetitle(),MainPageData.get(CHANGE_USERNAME) );
	}

	@Test(description = "This Test Case is to Check the Title of ChangePassword Page", priority=7,groups = { "sprint2" })
	public void ToVerifyChangePasswordPageTitle() {

		mainPage.clickOnChangePasswordLink();
		mainPageAssert.assertEquals(SeleniumUtil.getPagetitle(),MainPageData.get(CHANGE_PASSWORD) );
	}

	@Test(description = "This Test Case is to Check the Title of GrantAccess Page", priority=8,groups = { "sprint2" })
	public void ToVerifyGrantAccessPageTitle() {

		mainPage.clickOnGrantAccessLink();
		mainPageAssert.assertEquals(SeleniumUtil.getPagetitle(),MainPageData.get(GRANT_ACCESS) );
	}

	@Test(description = "This Test Case is to Check the Title of HistoryLog Page", priority=9,groups = { "sprint2" })
	public void ToVerifyHistoryLogPageTitle() {

		mainPage.clickOnHistoryLogLink();
		mainPageAssert.assertEquals(SeleniumUtil.getPagetitle(),MainPageData.get(HISTORY_LOG) );
	}

	@Test(description = "This Test Case is to Check the Title of LoginPage Page", priority=10,groups = { "sprint2", "sprint2" })
	public void ToVerifyLoginPageTitle() {

		mainPage.clickLogOffLink();
		mainPageAssert.assertEquals(SeleniumUtil.getPagetitle(),MainPageData.get(LOG_OFF) );
	}
	
	   @AfterClass(alwaysRun = true)
		public void TearDown(){
		   mainPage = null;
	    	
	    }

}
