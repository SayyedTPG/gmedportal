package com.rt.test;

import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;
import com.rt.pages.authenticated.MainPage;
import com.rt.test.base.AuthBaseTestClass;
import com.rt.utils.SeleniumUtil;
import com.rt.pages.authenticated.MessagePage;

public class MessagePageTest extends AuthBaseTestClass {

	private static final String VALID_USERID 	= "validUserName";
	private static final String VALID_PASSWORD 	= "validPassword";
	private static final String MESSAGE			="message1";
	private static final String SUBJECT 		="subject1";
	
	private MessagePage messagePage;
	private Assertion MessageAssert = new Assertion();
	private Map<String, String> MessageData;
	MainPage mainPage;
	
	@BeforeClass
	public void initClass() throws InterruptedException{
		MessageData = readexcelsheet("MessageData");
		mainPage = new MainPage();
		messagePage = new MessagePage();
		doLogin(MessageData.get(VALID_USERID),MessageData.get(VALID_PASSWORD));
		mainPage.SentMessages(); 
	}
	
	@Test
	public void toVerifySentMessage(){
		
		messagePage.ClickSentMessageButton();
		messagePage.TypeSubjectAndMessageBodyAndClickSentBtn(MessageData.get(SUBJECT), MessageData.get(MESSAGE));
		MessageAssert.assertEquals(MessageData.get(MESSAGE),messagePage.ExpectedMessage() );
	}
	
	   @AfterClass(alwaysRun = true)
		public void TearDown(){
		   messagePage = null;
		   mainPage = null;
		   
	    }
}
