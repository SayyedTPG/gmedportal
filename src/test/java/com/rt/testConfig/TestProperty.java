package com.rt.testConfig;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.rt.utils.PropertyFileReader;

/**
 * Test Properties are defined in this class
 * This class reads the Automation properties from automation.properties through ProperthFileReader Class
 * @author sayyed.akram
 *
 */
public class TestProperty {

	/** propObj is an instance of PropertyFileReader class */
	public static final PropertyFileReader prop = new PropertyFileReader();
	/** Instance of the properties */
	final static Properties propObj = prop.returnProperties("automation");
	
	/** TEST_URL contains the URL which is used for test Automation */
	public static final String TESTURL = propObj.getProperty("TESTURL");
	public static final int WAITTIME = Integer.parseInt(propObj.getProperty("WAITING_TIME"));
	public static final String BROWSER = propObj.getProperty("BROWSER");
	public static final String WORKBOOKNAME = propObj.getProperty("WORKBOOKNAME");
	public static final String SHEETNAME = propObj.getProperty("SHEETNAME");
	public static final int ELEMENTWAITTIME = Integer.parseInt(propObj.getProperty("ELEMENT_WAIT_TIME"));
	
}
