package com.rt.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * This class contains the methods used to read from Excel File.
 * @author sayyed.akram
 *
 */
public class ExcelFileUtility {

	/**
	 * logs messages to the appenders
	 */
	private static Logger logger = LogManager.getLogger(com.rt.utils.ExcelFileUtility.class);
	/**
	 * This method reads test data from data file specified in config properties. 
	 * @param workbook name
	 * @param sheet name
	 * @return HashMap with the test values read from the test excel file
	 */
	public HashMap<String, String> readExcelFile(final String workBookName, final String sheetName){
		
		HashMap<String, String> dataMap = new HashMap<String, String>();
		try {
			
			XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(new File(System.getProperty("user.dir") + "/src/test/resources/"+workBookName)));
			XSSFSheet sheet = workbook.getSheet(sheetName);
			FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
			String mapKey = new String();
			String mapValue = new String();
			int rowCounter = 0;
			
			logger.info("...reading the data from the excel data sheet...");
			
			Iterator<Row> rows = sheet.rowIterator();
			
			while (rows.hasNext()){
				XSSFRow row = (XSSFRow) rows.next();
				rowCounter = 0;
				String cellText = null;
				Iterator<Cell> cells = row.cellIterator();
				
				while(cells.hasNext()){
					
					XSSFCell cell = (XSSFCell) cells.next();
					if(cell.getCellType()==Cell.CELL_TYPE_STRING){
						cellText = cell.getRichStringCellValue().getString();
					}
					else if(cell.getCellType()==cell.CELL_TYPE_NUMERIC){
						cellText = String.valueOf((int) cell.getNumericCellValue());
					}
					else if(cell.getCellType()==cell.CELL_TYPE_BOOLEAN){
						cellText = String.valueOf(cell.getStringCellValue());
					}
					else if(cell.getCellType()==cell.CELL_TYPE_FORMULA){
						cellText = evaluator.evaluate(cell).getStringValue();
					}
					else if(cell.getCellType()==cell.CELL_TYPE_BLANK){
						cellText=null;
						continue;
					}
					else {
						logger.info("Not a valid type");
					}
					if(rowCounter==0){
						mapKey=cellText;
						rowCounter=rowCounter+1;
					}
					else if(rowCounter==1){
						mapValue=cellText;
						rowCounter=rowCounter+1;
					}
					dataMap.put(mapKey, mapValue);
				}
				
			}
			workbook.close();
			workbook = null;
			sheet = null;
		}catch (IOException e){
			logger.error("***ERROR*** Unable to read from the excel file "+workBookName);
			e.printStackTrace();
		}finally {
			logger.info("***ERROR***");
		}
		
		return dataMap;
	}
	
}
