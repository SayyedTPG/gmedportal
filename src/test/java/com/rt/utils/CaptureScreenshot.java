package com.rt.utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

public class CaptureScreenshot {

	
	public static void captureScrenShot(WebDriver driver, String screenshotname){
		
		try {
			
			//TakesScreenshot : interface 
			TakesScreenshot ts = (TakesScreenshot)driver;	
			//OutputType : Interface, Capture the screenshot and store it in the specified location.
			File source = ts.getScreenshotAs(OutputType.FILE);
			//FileUtils : Class. General file manipulation utilities.
			FileUtils.copyFile(source, new File ("./Screenshots/"+screenshotname+".png"));
			System.out.println("Screen Shot Taken");
		} catch (Exception e) {
			
			System.out.println("Exception while taking screen shot" + e.getMessage());
		}
	}
}
