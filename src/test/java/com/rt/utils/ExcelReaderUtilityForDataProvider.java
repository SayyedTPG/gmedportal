package com.rt.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

public class ExcelReaderUtilityForDataProvider {

	 	
	public  Object [][] getTestData(String workBookName, String sheetName) throws IOException {
		
			XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(new File(System.getProperty("user.dir") +"/src/test/resources/"+workBookName)));
			XSSFSheet worksheet = workbook.getSheet(sheetName);
		
		
		int rowcount = worksheet.getPhysicalNumberOfRows();
		
		Object [][] data = new Object [rowcount][2];
		
		for (int i=0; i<rowcount;i++){
			
			XSSFRow row = worksheet.getRow(i);
			XSSFCell username = row.getCell(0);
			data[i][0] = username.getStringCellValue();
			
			XSSFCell password = row.getCell(1);
			data[i][1] = password.getStringCellValue();
		}
		workbook.close();
		workbook = null;
		worksheet = null;
		System.out.println(data);
		return data;
		
		
		
}
}
	
