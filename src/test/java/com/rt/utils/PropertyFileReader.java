package com.rt.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PropertyFileReader {
	
	/** Logger to log the Driver Factory log messages */
	public static Logger logger = LogManager.getLogger(com.rt.utils.PropertyFileReader.class);
	
	/**
	 * @param propertyFileName, Key
	 * @return Value of the corresponding Key of propertyFile 
	 */
	public String returnPropVal (final String propertyFileName, final String key ){
		
		final Properties properties = new Properties();
		String value = null;
		{
			
			try{
				properties.load(new FileInputStream(new File(System.getProperty("user.dir")+"/src/test/resources/automation.properties")));
			
				value = properties.getProperty(key);
				System.out.println(value);
			}catch (final FileNotFoundException e){
				logger.error("The File not found at "+"/src/test/resources"+ propertyFileName+".properties",e);
			}catch (final IOException e){
				logger.error("IOException was found in returnPropValue method", e);
			}
			
		}
		return value;
		
	}
	/**
	 * @param propertyFileName
	 * @return A PROPERTY File containing automation key value pair
	 */
	
	public Properties returnProperties(final String propertyFileName){
		
		// get a new properties object:
		
		Properties properties = new Properties();
		{
			try{
				properties.load(new FileInputStream(new File(System.getProperty("user.dir")+"/src/test/resources/automation.properties")));
				
			}catch (FileNotFoundException e){
				
				logger.error("The file was not found at " + "/src/test/resources"+ propertyFileName + ".properties", e);
			}catch (IOException e){
				
				logger.error("IOException was found in returnProperties method", e);
			}
		}
		return properties;
	}

}
