package com.rt.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static com.rt.testConfig.TestProperty.ELEMENTWAITTIME;
import static com.rt.testConfig.TestProperty.WAITTIME;
import com.rt.testConfig.TestProperty;
import static com.rt.helper.DriverFactory.driver;

public class SeleniumUtil {
	
	private static Logger logger = LogManager.getLogger(com.rt.utils.SeleniumUtil.class);
	
	public static WebElement element (By elementToken, WebDriver driver ) {
		
		return driver.findElement(elementToken);

	}
	
	public static boolean isElementEnabled (By elementLocator, WebDriver driver){
		
		try{
		driver.findElement(elementLocator).isEnabled();
		}
			catch (Exception e) {
				
				logger.info("Unable to locate Element:"+elementLocator);
				return false;
			}
			
		return true;
		}
	
	public static boolean isElementPresent(By fieldLocator, WebDriver driver) {
		try {
			logger.debug("Element: " + fieldLocator);
			driver.findElement(fieldLocator);

		} catch (org.openqa.selenium.NoSuchElementException Ex) {
			logger.debug("Unable to locate Element: " + fieldLocator);
			return false;
		}
		return true;
	}
	
/**
 * This method is used for wait for element for ELEMENT_POLL_TIME defined in
 * TestProperty file
 * 
 * @param elementLocator
 * @param driver
 */
	
	public static void waitForElementToBePresent(By elementLocator, WebDriver driver) {
		WebElement myDynamicElement = (new WebDriverWait(driver, TestProperty.WAITTIME))
				.until(ExpectedConditions.presenceOfElementLocated(elementLocator));
		if (myDynamicElement.isDisplayed()) {
			logger.info("Element Found: " + elementLocator.toString());
		} else {
			logger.error("\nElement NOT found: " + elementLocator.toString());
		}
	}
	
	public static void sendKeysToWebElement(By locator,String text,WebDriver driver){
		try{
			element(locator, driver).clear();
			element(locator, driver).sendKeys(text);
			logger.debug("Input following text: "+text+" | Identifier: "+locator);
		}catch(Exception e){
			logger.debug("**ERROR**Failed to send text into field.  Text: "+text+ " | Identifier: "+locator);
			e.getStackTrace();
		}
	}
	public static void click(By xpath,WebDriver driver){
		new WebDriverWait(driver,WAITTIME).until(ExpectedConditions.
				elementToBeClickable(xpath));
		logger.debug("Sucessfully Found Element : " + xpath);
		driver.findElement(xpath).click();
		logger.debug("Sucessfully clicked on element : " + xpath);
	}
	
	public static void waitForPageLoad(WebDriver driver) {
		ExpectedCondition<Boolean> pageLoadCondition = new
				ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(pageLoadCondition);
	}
	
	public static void waitForElementToBeVisible(By elementLocator, WebDriver driver) {
		try{
			WebElement myDynamicElement = (new WebDriverWait(driver, TestProperty.ELEMENTWAITTIME))
					.until(ExpectedConditions.visibilityOfElementLocated(elementLocator));
		}
		catch(Exception e){
			logger.info("Exception occured.. "+ e);
		}
	}
	
	public static String getPagetitle(){
		
		return driver.getTitle();
		
	}
	
}
