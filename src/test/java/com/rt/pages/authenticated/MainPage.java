package com.rt.pages.authenticated;

import static com.rt.helper.DriverFactory.driver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.rt.utils.ExcelFileUtility;
import com.rt.utils.SeleniumUtil;

public class MainPage {

	private By homePageLink = By.xpath("//ul[@id='navigation-menu']//a[@href='/PP65/']");
	private By MessageLink = By.xpath("//ul[@id='navigation-menu']//a[contains(text(),'Messages')]");
	private By healthSummaryLink = By.xpath("//ul[@id='navigation-menu']//a[@href='/PP65/Patient/HealthSummary']");
	private By mydocumentLink = By
			.xpath("/ul[@id='navigation-menu']//a[@href='/PP65/Document/List?onlyNew=False&source=MyDocuments']");
	private By billingLink = By.xpath("//ul[@id='navigation-menu']//li[5]/a");
	private By onLineBillPayLink = By.xpath("//ul[@id='navigation-menu']//a[@href='/PP65/Patient/BillPay']");
	private By myAccountLink = By.xpath("//ul[@id='navigation-menu']//li[6]/a");
	private By changeUserNameLink = By.xpath("//ul[@id='navigation-menu']//a[@href='/PP65/Account/ChangeUserName']");
	private By changePasswordLink = By.xpath("//ul[@id='navigation-menu']//a[@href='/PP65/Account/ChangePassword']");
	private By grantAccessLink = By.xpath("//ul[@id='navigation-menu']//a[@href='/PP65/Account/AlternateUser']");
	private By historyLogLink = By.xpath("//ul[@id='navigation-menu']//a[@href='/PP65/AccessLog/AccessLog']");
	private By LogoffLink = By.xpath("//ul[@id='navigation-menu']//a[@href='/PP65/Account/LogOff']");
	private By SentMessageLink = By.xpath("//ul[@id='navigation-menu']//a[@href='/PP65/Messaging/Sent']");

	private static Logger logger = LogManager.getLogger(com.rt.pages.authenticated.MainPage.class);

	/**
	 * Home Page methods that will be used by other pages.
	 * 
	 * @throws InterruptedException
	 */

	public void clickHomePageLink() {

		SeleniumUtil.waitForPageLoad(driver);
		SeleniumUtil.click(homePageLink, driver);

	}

	public void clickOnSentMessagesLink() {

		SeleniumUtil.click(MessageLink, driver);
		SeleniumUtil.click(SentMessageLink, driver);
	}

	public void SentMessages() {
		
		SeleniumUtil.click(MessageLink, driver);
		SeleniumUtil.click(SentMessageLink, driver);
	}

	public void clickOnHealthSummaryLink() {

		SeleniumUtil.click(healthSummaryLink, driver);

	}

	public void clickOnMydocumentLink() {

		SeleniumUtil.click(mydocumentLink, driver);
	}

	public void clickBillingLink() {

		SeleniumUtil.click(billingLink, driver);
	}

	public void clickOnBillPayLink(){

		SeleniumUtil.click(onLineBillPayLink, null);

	}

	public void clickOnMyAccountLink() {

		SeleniumUtil.click(myAccountLink, driver);
	}

	public void clickOnChangeUserNameLink() {

		SeleniumUtil.click(changeUserNameLink, driver);

	}

	public void clickOnChangePasswordLink() {

		SeleniumUtil.click(changePasswordLink, driver);

	}

	public void clickOnGrantAccessLink() {

		SeleniumUtil.click(grantAccessLink, driver);

	}

	public void clickOnHistoryLogLink() {

		SeleniumUtil.click(historyLogLink, driver);

	}

	public void clickLogOffLink() {

		SeleniumUtil.waitForElementToBePresent(LogoffLink, driver);
		SeleniumUtil.click(LogoffLink, driver);

	}

}
