package com.rt.pages.authenticated;

import static com.rt.helper.DriverFactory.driver;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import com.rt.utils.SeleniumUtil;

public class MessagePage {

	
	private By sentMsgTxt 			= By.xpath("//div[@id='sentMessageContainer']/div[2]/div[1]/span");
	private By sendAMessageBtn	 	= By.xpath("//button[@id='messageSendNew']");
	private By subjectTxtBox 		= By.xpath("//input[@id='Subject']");
	private By messageBodyTxtArea 	= By.xpath("//textarea[@id='Message']");
	private By sendBtn 				= By.id("messageSendNew");

	
	private static Logger logger 	= LogManager.getLogger(com.rt.pages.authenticated.MessagePage.class);
	
	public void ClickSentMessageButton(){
		SeleniumUtil.waitForElementToBeVisible(sendAMessageBtn, driver);
		SeleniumUtil.element(sendAMessageBtn, driver).click();
	}
	
	public void TypeSubjectAndMessageBodyAndClickSentBtn(String subject, String messagebody){
		
		SeleniumUtil.waitForElementToBePresent(subjectTxtBox, driver);
		SeleniumUtil.sendKeysToWebElement(subjectTxtBox, subject, driver);
		SeleniumUtil.sendKeysToWebElement(messageBodyTxtArea, messagebody, driver);
		SeleniumUtil.click(sendBtn, driver);	
	}
	
	public String ExpectedMessage(){
		
		return SeleniumUtil.element(sentMsgTxt, driver).getText();
	}
	
	
	
}
