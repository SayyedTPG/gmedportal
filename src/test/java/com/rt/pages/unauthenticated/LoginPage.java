package com.rt.pages.unauthenticated;

import static com.rt.helper.DriverFactory.driver;
import static com.rt.utils.SeleniumUtil.sendKeysToWebElement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

import com.rt.test.base.BaseTestClass;
import com.rt.utils.SeleniumUtil;

public class LoginPage extends BaseTestClass {
	
	
	/**
	 * Logger to log messages
	 */
	private static Logger logger = LogManager.getLogger(com.rt.pages.unauthenticated.LoginPage.class);
	
	
		private By usernameTxt			= By.xpath("//input[@id='UserName']");
		private By passwordTxt			= By.xpath("//input[@id='Password']") ;
		private By signinBtn			= By.xpath("//input[@value='Sign in']");
		
		
		public void setUsername(final String username) {
			sendKeysToWebElement(usernameTxt,username,driver);
			logger.info("Entered the Username: " + username);
		}
		
		public void setPassword(final String password) {
			sendKeysToWebElement(passwordTxt,password,driver);
			logger.info("Entered the Password: " + password);
		}
		
		public void submitLogin() {
			SeleniumUtil.waitForElementToBePresent(signinBtn, driver);
			SeleniumUtil.element(signinBtn, driver).click();
			logger.info("...logging into Client Portal. ||");
		}
		
		public void loginAs(final String username, final String password) {
			logger.info("...logging into the application...");
			setUsername(username);
			setPassword(password);
			submitLogin();
			logger.info("Logged into the application...");
		}
		
		
}
