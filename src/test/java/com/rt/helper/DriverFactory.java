package com.rt.helper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import static com.rt.testConfig.TestProperty.TESTURL;
import static com.rt.testConfig.TestProperty.WAITTIME;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.rt.testConfig.TestProperty;

public class DriverFactory {

	
	public static String browser = TestProperty.BROWSER;
	/** WebDriver instance to create Webdriver*/
	public static WebDriver webDriver;
	/** Event firing Webdriver instance*/
	public static EventFiringWebDriver driver = null;
	/** Logger to log the Driver Factory log message*/
	private static Logger logger					=LogManager.getLogger(com.rt.helper.DriverFactory.class);
	
	/**
	 * This method is called by the test classes to create driver
	 */
	
	public static void getDriverInstance(){
		createDriver();
		logger.info("Initialized the webdriver");
	}
	
	/**
	 * This is the factory method used for creating appropriate web driver
	 * 
	 * @param browserId - Browser to run the Test on
	 */
	
	public static EventFiringWebDriver createDriver(){
		
		logger.info("This browser to open is " + TestProperty.BROWSER);
		
		if ("firefox".equalsIgnoreCase(TestProperty.BROWSER.trim())) {
		logger.info("Creating the instance of firefoxDriver");
		webDriver = new FirefoxDriver();
		}
		else if ("ie".equalsIgnoreCase(TestProperty.BROWSER.trim())) {
			
			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, TESTURL);
			logger.info("Creating the instance of wendriver IE driver");
			
			File file = new File("src/test/resources/drivers/IEDriverServer.exe");
			System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
			
			webDriver = new InternetExplorerDriver(cap);
			
		}
		else if ("chrome".equalsIgnoreCase(TestProperty.BROWSER)){
			
			logger.info("Creating the instance of webdriver.chrome.driver");
			
			final File file = new File("src/test/resources/drivers/chromedriver.exe");
			
			System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
			
			 DesiredCapabilities capability = DesiredCapabilities.chrome();
		        ChromeOptions options = new ChromeOptions();
		        options.addArguments("test-type");
		        options.addArguments("--disable-web-security");
		        options.addArguments("--no-proxy-server");
		        capability.setCapability("chrome.binary", file.getAbsolutePath());
		        capability.setCapability(ChromeOptions.CAPABILITY, options);
		        Map<String, Object> prefs = new HashMap<String, Object>();
		        prefs.put("credentials_enable_service", false);
		        prefs.put("profile.password_manager_enabled", false);
		        options.setExperimentalOption("prefs", prefs);
		        capability.setCapability("chrome.binary", file.getAbsolutePath());
		        capability.setCapability(ChromeOptions.CAPABILITY, options);
		        webDriver = new ChromeDriver(capability);
		}
		else {
			try {
					throw new Exception(TestProperty.BROWSER + "Not supported. Choose either FireFox, ie or chrome");
			}catch (Exception e){
				logger.error("The requested browser is NOT supported! Choose either firefox, ie, or chrome.", e);
			}
		}
		
		return 
				driver = new EventFiringWebDriver(webDriver);
		
	}
	
	/**
	 * This method opens the URL and sets the driver properties
	 */
	
	public static void openURL(String url){
		logger.info("URL to open" + url);
		
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(WAITTIME, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
	}
}
